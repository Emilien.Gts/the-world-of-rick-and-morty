import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:the_world_of_rick_and_morty/model/episode/episode_model.dart';

class EpisodeApi {
  static Future<EpisodeModel> getEpisode(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);
      return EpisodeModel.fromJson(responseJson);
    } else {
      throw Exception('Unable to get characters from API');
    }
  }
}
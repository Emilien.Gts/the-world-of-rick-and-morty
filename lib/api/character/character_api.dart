import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:the_world_of_rick_and_morty/helper/character/character_helper.dart';
import 'package:the_world_of_rick_and_morty/model/character/character_model.dart';
import 'package:the_world_of_rick_and_morty/model/info/info_model.dart';

class CharacterApi {
  static final String url = "https://rickandmortyapi.com/api/character/";

  static Future<List<CharacterModel>> getCharacters() async {
    InfoModel info = await CharacterApi.getCharactersInfo();
    var listOfCount = CharacterHelper.transformCountofCharacters(info.count);

    final response = await http.get(CharacterApi.url + listOfCount);

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);
      return (responseJson as List)
          .map((p) => CharacterModel.fromJson(p))
          .toList();
    } else {
      throw Exception('Unable to get characters from API');
    }
  }

  static Future<CharacterModel> getCharacter(int id) async {
    final response = await http.get(CharacterApi.url + id.toString());

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);
      return CharacterModel.fromJson(responseJson);
    } else {
      throw Exception('Unable to get characters from API');
    }
  }

  static Future<InfoModel> getCharactersInfo() async {
    final response = await http.get(CharacterApi.url);

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);
      return InfoModel.fromJson(responseJson['info']);
    } else {
      throw Exception('Unable to get characters from API');
    }
  }
}
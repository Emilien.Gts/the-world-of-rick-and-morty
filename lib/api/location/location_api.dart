import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:the_world_of_rick_and_morty/model/location/location_model.dart';

class LocationApi {
  static Future<LocationModel> getLocation(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);
      return LocationModel.fromJson(responseJson);
    } else {
      throw Exception('Unable to get characters from API');
    }
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_world_of_rick_and_morty/style/colors.dart';

class Button extends StatelessWidget {
  String _text;
  Function _onPressed;

  Button(this._text, this._onPressed);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0)
      ),
      color: blue,
      height: 10.0,
      textColor: white,
      splashColor: blueAccent,
      padding: EdgeInsets.all(16.0),
      onPressed: this._onPressed,
      child: Text(this._text),
    );
  }
}

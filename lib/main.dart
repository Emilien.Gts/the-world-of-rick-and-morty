import 'package:flutter/material.dart';
import 'package:the_world_of_rick_and_morty/page/character/characters_page.dart';
import 'package:the_world_of_rick_and_morty/style/themes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: appTheme,
      home: CharactersPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

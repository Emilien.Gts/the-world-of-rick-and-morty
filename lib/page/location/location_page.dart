import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_world_of_rick_and_morty/api/location/location_api.dart';
import 'package:the_world_of_rick_and_morty/model/location/location_model.dart';
import 'package:the_world_of_rick_and_morty/style/text.dart';

class LocationPage extends StatefulWidget {
  final String name;
  final String url;

  const LocationPage({Key key, this.name, this.url}) : super(key: key);

  @override
  _LocationPageState createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  Future<LocationModel> _location;

  @override
  void initState() {
    super.initState();
    print(widget.url);
    _location = LocationApi.getLocation(widget.url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name == 'unknown' ? 'Unknown' : widget.name),
      ),
      body: Container(
        child: FutureBuilder<LocationModel>(
          future: _location,
          builder: _getLocationFromApi,
        ),
      ),
    );
  }

  Widget _getLocationFromApi(BuildContext context, AsyncSnapshot<LocationModel> snapshot) {
    if(snapshot.connectionState == ConnectionState.waiting) {
      return Center(child: CircularProgressIndicator());
    }else if(snapshot.hasError) {
      return Center(
        child: Text("The place of origin of this character is not known."),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(snapshot.data.name, style: h1,),
              SizedBox(
                height: 25.0,
              ),
              Text("Type: " + snapshot.data.type),
              SizedBox(
                height: 10.0,
              ),
              Text("Dimension: " + snapshot.data.dimension),
            ],
          ),
        ),
      );
    }
  }
}

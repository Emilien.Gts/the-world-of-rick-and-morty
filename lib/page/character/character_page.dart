import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_world_of_rick_and_morty/api/character/character_api.dart';
import 'package:the_world_of_rick_and_morty/model/character/character_location_model.dart';
import 'package:the_world_of_rick_and_morty/model/character/character_origin_model.dart';
import 'package:the_world_of_rick_and_morty/page/episode/episodes_page.dart';
import 'package:the_world_of_rick_and_morty/page/location/location_page.dart';
import 'package:the_world_of_rick_and_morty/widget/button.dart';
import 'package:the_world_of_rick_and_morty/model/character/character_model.dart';
import 'package:the_world_of_rick_and_morty/style/colors.dart';
import 'package:the_world_of_rick_and_morty/style/text.dart';

class CharacterPage extends StatefulWidget {
  final int id;

  const CharacterPage ({ Key key, this.id }): super(key: key);

  @override
  _CharacterPageState createState() => _CharacterPageState();
}

class _CharacterPageState extends State<CharacterPage> {
  Future<CharacterModel> _character;

  @override
  void initState() {
    super.initState();
    _character = CharacterApi.getCharacter(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Character #" + widget.id.toString()),
      ),
      body: Container(
        child: FutureBuilder<CharacterModel>(
          future: _character,
          builder: _getCharacterFromApi,
        ),
      ),
    );
  }

  Widget _getCharacterFromApi(BuildContext context, AsyncSnapshot<CharacterModel> snapshot) {
    if(snapshot.connectionState == ConnectionState.waiting) {
      return Center(child: CircularProgressIndicator());
    }else if(snapshot.hasError) {
      return Text(snapshot.error);
    } else {
      return CharacterPageContent(
        snapshot.data.image,
        snapshot.data.name,
        snapshot.data.status,
        snapshot.data.species,
        snapshot.data.type,
        snapshot.data.gender,
        snapshot.data.characterOriginModel,
        snapshot.data.characterLocationModel,
        List<String>.from((snapshot.data.episodes))
      );
    }
  }

}

class CharacterPageContent extends StatelessWidget {
  String _image;
  String _name;
  String _status;
  String _species;
  String _type;
  String _gender;
  CharacterOriginModel _origin;
  CharacterLocationModel _location;
  List<String> _episodes;

  CharacterPageContent(this._image, this._name, this._status, this._species,
      this._type, this._gender, this._origin, this._location, this._episodes);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 25.0, right: 6.0, bottom: 25.0, left: 6.0),
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100.0),
                child: Image.network(
                    this._image
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Expanded(
            flex: 1,
            child: Text(this._name, style: h1,),
          ),
          Expanded(
            flex: 2,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(right: 16.0, left: 16.0),
                child: Container(
                  child: RichText(
                    text: TextSpan(
                        text: 'This character is a(n): ',
                        style: TextStyle(color: black, fontSize: 16),
                        children: <TextSpan>[
                          TextSpan(
                              text: this._species == "" ? "Unknown specie": this._species,
                              style: TextStyle(color: Colors.blueAccent, fontSize: 16)
                          ),
                          TextSpan(
                              text: ', he is: ',
                              style: TextStyle(color: black, fontSize: 16)
                          ),
                          TextSpan(
                              text: this._status == "unknown" ? "Alive or Dead, we don't know": this._status,
                              style: TextStyle(color: Colors.blueAccent, fontSize: 16)
                          ),
                          TextSpan(
                              text: '. He is more particulary one: ',
                              style: TextStyle(color: black, fontSize: 16)
                          ),
                          TextSpan(
                              text: this._type == "" ? "Not informed": this._type,
                              style: TextStyle(color: Colors.blueAccent, fontSize: 16)
                          ),
                          TextSpan(
                              text: ' and his gender is: ',
                              style: TextStyle(color: black, fontSize: 16)
                          ),
                          TextSpan(
                              text: (this._gender == "Unknown" ? "": this._gender) + '.',
                              style: TextStyle(color: Colors.blueAccent, fontSize: 16)
                          ),
                        ]
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Expanded(
                    flex: 2,
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Button(
                            'Place of origin',
                            () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) {
                                return LocationPage(name: this._origin.name, url: this._origin.url);
                              }));
                            }
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: Button(
                            'Last seen',
                              () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return LocationPage(name: this._location.name, url: this._location.url);
                                }));
                              }
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Button(
                      'Appearance(s) in the series',
                            () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) {
                                return EpisodesPage(episodes: this._episodes, name: this._name);
                              }));
                            }
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}


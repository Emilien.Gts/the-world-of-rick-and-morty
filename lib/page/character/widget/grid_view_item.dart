import 'package:flutter/cupertino.dart';
import 'package:the_world_of_rick_and_morty/style/widget/grid_view_item.dart';

class GridViewItem extends StatelessWidget {
  String _image;
  String _name;

  GridViewItem(this._image, this._name);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: gridViewItemDecoration,
        margin: const EdgeInsets.all(12.0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Expanded(
                flex: 2,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50.0),
                  child: Image.network(this._image),
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(this._name,
                      textAlign: TextAlign.center, style: gridViewTextStyle),
                ),
              ),
            ],
          ),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:the_world_of_rick_and_morty/api/character/character_api.dart';
import 'package:the_world_of_rick_and_morty/model/character/character_model.dart';
import 'package:the_world_of_rick_and_morty/page/character/widget/grid_view_item.dart';

import 'character_page.dart';

class CharactersPage extends StatefulWidget {
  @override
  _CharactersPageState createState() => _CharactersPageState();
}

class _CharactersPageState extends State<CharactersPage> {
  Future<List<CharacterModel>> _characters;

  @override
  void initState() {
    super.initState();
    _characters = CharacterApi.getCharacters();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text("The world of Rick and Morty"),
        ),
      ),
      body: Container(
        child: FutureBuilder<List<CharacterModel>>(
          future: _characters,
          builder: _getCharactersFromApi,
        ),
      ),
    );
  }

  Widget _getCharactersFromApi(BuildContext context,
      AsyncSnapshot<List<CharacterModel>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return Center(child: CircularProgressIndicator());
    } else if (snapshot.hasError) {
      return Text(snapshot.error);
    } else {
      return Padding(
        padding: const EdgeInsets.all(6.0),
        child: GridView.builder(
          itemCount: snapshot.data.length,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return CharacterPage(id: snapshot.data[index].id);
                  }));
                },
                child: GridViewItem(
                    snapshot.data[index].image, snapshot.data[index].name)
            );
          },
        ),
      );
    }
  }
}

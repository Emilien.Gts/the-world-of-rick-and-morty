import 'package:flutter/material.dart';
import 'package:the_world_of_rick_and_morty/api/episode/episode_api.dart';
import 'package:the_world_of_rick_and_morty/model/episode/episode_model.dart';
import 'package:the_world_of_rick_and_morty/style/text.dart';

class EpisodesPage extends StatefulWidget {
  final List<String> episodes;
  final String name;

  const EpisodesPage({Key key, this.episodes, this.name}) : super(key: key);

  @override
  _EpisodesPageState createState() => _EpisodesPageState();
}

class _EpisodesPageState extends State<EpisodesPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name + "'s episodes"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Text("Number of episodes: " + widget.episodes.length.toString(), style: h1,),
                ),
                Expanded(
                  flex: 9,
                  child: ListView(
                    children: [
                      for(var i = 0; i < widget.episodes.length; i++)
                        Container(
                          child: ListViewItem(url: widget.episodes[i]),
                        )
                    ],
                  ),
                )
              ],
            )
        ),
      ),
    );
  }
}

class ListViewItem extends StatefulWidget {
  final String url;

  const ListViewItem({Key key, this.url}) : super(key: key);

  @override
  _ListViewItemState createState() => _ListViewItemState();
}

class _ListViewItemState extends State<ListViewItem> {
  Future<EpisodeModel> _episode;

  @override
  void initState() {
    super.initState();
    print(widget.url.toString().runtimeType);
    _episode = EpisodeApi.getEpisode(widget.url.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<EpisodeModel>(
        future: _episode,
        builder: _getEpisodeFromApi,
      ),
    );
  }

  Widget _getEpisodeFromApi(BuildContext context, AsyncSnapshot<EpisodeModel> snapshot) {
    if(snapshot.connectionState == ConnectionState.waiting) {
      return Center(child: CircularProgressIndicator());
    }else if(snapshot.hasError) {
      return Center(
        child: Text(snapshot.error),
      );
    } else {
      return Padding(
          padding: EdgeInsets.only(bottom: 16.0),
          child: Card(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
              child: Text(snapshot.data.name, style: TextStyle(
                fontSize: 18.0,
                height: 1.6,
              ),),
            ),
          )
      );
    }
  }
}

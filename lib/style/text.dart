import 'package:flutter/material.dart';

import 'colors.dart';

// Title
final TextStyle h1 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 20.0
);

// Text
final TextStyle bold = TextStyle(
  color: black,
  fontWeight: FontWeight.w400
);

final TextStyle italic = TextStyle(
    color: black,
  fontStyle: FontStyle.italic
);
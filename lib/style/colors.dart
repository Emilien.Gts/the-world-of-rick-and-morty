import 'package:flutter/material.dart';

// White
final Color white = Colors.white;

// Black
final Color black = Colors.black;

// Gray
final Color gray81 = Color.fromRGBO(207, 207, 207, 1);
final Color gray98 = Color.fromRGBO(250, 250, 250, 1);

// Blue
Color blue = Colors.blue;
Color blueAccent = Colors.blueAccent;

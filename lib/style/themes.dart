import 'package:flutter/material.dart';

import 'colors.dart';

final ThemeData appTheme = ThemeData(
  accentColor: Colors.black,
  textTheme: _textTheme,
  scaffoldBackgroundColor: gray98
);

final TextTheme _textTheme = TextTheme(
    headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
);
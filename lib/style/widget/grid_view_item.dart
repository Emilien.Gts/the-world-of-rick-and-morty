import 'package:flutter/material.dart';

import '../colors.dart';

// Container
BoxDecoration gridViewItemDecoration = BoxDecoration(
  color: white,
  borderRadius: BorderRadius.all(Radius.circular(6.0)),
);

// Text
TextStyle gridViewTextStyle = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 16.0
);
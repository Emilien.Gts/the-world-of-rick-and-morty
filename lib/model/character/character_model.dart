import 'character_location_model.dart';
import 'character_origin_model.dart';

class CharacterModel {
  int id;
  String name;
  String status;
  String species;
  String type;
  String gender;
  CharacterOriginModel characterOriginModel;
  CharacterLocationModel characterLocationModel;
  String image;
  List<dynamic> episodes;
  String url;
  String created;

  CharacterModel(
      this.id,
      this.name,
      this.status,
      this.species,
      this.type,
      this.gender,
      this.characterOriginModel,
      this.characterLocationModel,
      this.image,
      this.episodes,
      this.url,
      this.created);

  factory CharacterModel.fromJson(Map<String, dynamic> json) {
    return CharacterModel(
      json['id'],
      json['name'],
      json['status'],
      json['species'],
      json['type'],
      json['gender'],
      CharacterOriginModel.fromJson(json['origin']),
      CharacterLocationModel.fromJson(json['location']),
      json['image'],
      json['episode'],
      json['url'],
      json['created'],
    );
  }
}
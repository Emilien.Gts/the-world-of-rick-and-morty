class LocationModel {
  int id;
  String name;
  String type;
  String dimension;

  LocationModel(this.id, this.name, this.type, this.dimension);

  factory LocationModel.fromJson(Map<String, dynamic> json) {
    return LocationModel(
      json['id'],
      json['name'],
      json['type'],
      json['dimension'],
    );
  }
}
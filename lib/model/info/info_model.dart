class InfoModel {
  int count;
  int pages;
  String next;
  String prev;

  InfoModel(this.count, this.pages, this.next, this.prev);

  factory InfoModel.fromJson(Map<String, dynamic> json) {
    return InfoModel(
      json['count'],
      json['pages'],
      json['next'],
      json['prev'],
    );
  }
}
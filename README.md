<!-- NAME -->

## Name

The World of Rick and Morty

<!-- DESCRIPTION -->

## Description

This project is a mobile application based on the [Framework Flutter](https://flutter.dev/). This mobile application will allow you to discover a part of Rick and Morty's world, based on the API : Rick and Morty API](https://rickandmortyapi.com/).
You will be able to discover the list of the characters of the series as well as some details about them.

<!-- CONTEXT -->

## Context

This project was designed in a personal context. After having had Flutter lessons, I decided to make this application in order to continue learning the basics of Flutter with a concrete project.

<!-- GETTING STARTED -->

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

<!-- VISUALS -->

## Visuals

### Characters

<img src="https://gitlab.com/Emilien.Gts/the-world-of-rick-and-morty/-/raw/master/images/characters.jpg" width="350px">

### Detail of a character

<img src="https://gitlab.com/Emilien.Gts/the-world-of-rick-and-morty/-/raw/master/images/detail_of_a_character.jpg" width="350px">

### Place of origin

<img src="https://gitlab.com/Emilien.Gts/the-world-of-rick-and-morty/-/raw/master/images/place_of_origin.jpg" width="350px">

### Last seen

<img src="https://gitlab.com/Emilien.Gts/the-world-of-rick-and-morty/-/raw/master/images/last_seen.jpg" width="350px">

### Appearance(s) in the series

<img src="https://gitlab.com/Emilien.Gts/the-world-of-rick-and-morty/-/raw/master/images/appearance_in_the_series.jpg" width="350px">

<!-- LICENSE -->

## License

[License](https://gitlab.com/Emilien.Gts/the-world-of-rick-and-morty/-/blob/master/LICENSE)

<!-- PROJECT STATUS -->

## Project Status

This project is considered finished. The objective has been achieved: to display information based on the chosen API in order to improve Flutter skills.
